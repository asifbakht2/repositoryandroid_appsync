package com.spectrumbrand.appsync.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spectrumbrand.appsync.R;
import com.spectrumbrand.appsync.model.Device;

import java.util.List;

public class DeviceAdapter extends BaseAdapter {

    private final List<Device> devices;
    private final LayoutInflater mInflater;

    public DeviceAdapter(final Context context, final List<Device> devices) {
        this.devices = devices;
        this.mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return this.devices.size();
    }

    public Object getItem(int position) {
        return this.devices.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.device_item, null);
            holder = new ViewHolder();
            holder.deviceName = (TextView) convertView.findViewById(R.id.deviceNameId);
            holder.deviceStatus = (TextView) convertView.findViewById(R.id.deviceStatusId);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.deviceName.setText(this.devices.get(position).getDeviceName());
        holder.deviceStatus.setText(this.devices.get(position).getDeviceStatus());

        return convertView;
    }

    static class ViewHolder {
        private TextView deviceName;
        private TextView deviceStatus;
    }
}