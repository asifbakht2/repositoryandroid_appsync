package com.spectrumbrand.appsync.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;

import com.spectrumbrand.appsync.R;

public class LoadingDialog {

    private final Activity activity;
    private AlertDialog alertDialog;

    public LoadingDialog(final Activity activity) {
        this.activity = activity;
    }

    public void showDialog(final String message) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);

        final LayoutInflater inflater = activity.getLayoutInflater();
        alertBuilder.setView(inflater.inflate(R.layout.loading_dialog, null));
        alertBuilder.setCancelable(false);
        alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    public void closeDialog() {
        alertDialog.dismiss();;
    }

}
