package com.spectrumbrand.appsync;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.demo.posts.GetUserDevicesQuery;
import com.amazonaws.demo.posts.OnLockStatusChangeSubscription;
import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.spectrumbrand.appsync.adapter.DeviceAdapter;
import com.spectrumbrand.appsync.configuration.ClientFactory;
import com.spectrumbrand.appsync.configuration.Utils;
import com.spectrumbrand.appsync.model.Device;
import com.spectrumbrand.appsync.storage.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

public class DeviceActivity extends AppCompatActivity {

    private static final int LAUNCH_SECOND_ACTIVITY = 1;

    private ListView deviceListView;
    private List<Device> devices = new ArrayList<>();
    private AppSyncSubscriptionCall<OnLockStatusChangeSubscription.Data> subscriptionWatcher;
    private DeviceAdapter adapter = null;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        startSubscription();

        deviceListView = (ListView) findViewById(R.id.deviceList);
        adapter = new DeviceAdapter(this, devices);
        deviceListView.setAdapter(adapter);
        deviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Device device = devices.get(position);
                final Intent intent = new Intent(DeviceActivity.this, DeviceDetail.class).putExtra("deviceObject", device);
                startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
            }
        });
        email = SharedPrefManager.getInstance(DeviceActivity.this).getCredentials().getUsername();
//        final LoadingDialog loadingDialog = new LoadingDialog(DeviceActivity.this);
//        loadingDialog.showDialog("This shoudl replace message");
//        final Call<GenericResponse<Home>> homeRequest = RetrofitClient.getInstance().getApi().getHomes();
//        homeRequest.enqueue(new Callback<GenericResponse<Home>>() {
//            @Override
//            public void onResponse(Call<GenericResponse<Home>> call, Response<GenericResponse<Home>> response) {
//                final GenericResponse<Home> homesResponse = response.body();
//                if (homesResponse != null) {
//                    final List<Observable<?>> requests = new ArrayList<>();
//                    for (final Home home : (List<Home>) homesResponse.getData()) {
//                        requests.add(RetrofitClient.getInstance().getApi().getDevicesOfHome(home.getHomeid()));
//                    }
//                    Observable.zip(
//                            requests,
//                            new Function<Object[], GenericResponse<Device>>() {
//                                @Override
//                                public GenericResponse<Device> apply(Object[] objects) throws Exception {
//
//                                    GenericResponse<Device> finalObject = null;
//                                    for (Object object : objects) {
//                                        final GenericResponse<Device> deviceResponse = (GenericResponse<Device>) object;
//                                        finalObject = deviceResponse;
//                                        if (deviceResponse != null) {
//                                            devices.addAll((List<Device>) deviceResponse.getData());
//                                        }
//                                    }
//                                    return finalObject;
//                                }
//                            })
//                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(
//                                    // Will be triggered if all requests will end successfully (4xx and 5xx also are successful requests too)
//                                    new Consumer<Object>() {
//                                        @Override
//                                        public void accept(Object o) throws Exception {
//                                            //Do something on successful completion of all requests
//                                            renderDeviceInformation();
//                                            loadingDialog.closeDialog();
//                                        }
//                                    },
//
//                                    // Will be triggered if any error during requests will happen
//                                    new Consumer<Throwable>() {
//                                        @Override
//                                        public void accept(Throwable e) throws Exception {
//                                            //Do something on error completion of requests
//                                        }
//                                    }
//                            );
//                } else {
//                    loadingDialog.closeDialog();
//                    Toast.makeText(DeviceActivity.this, "No homes", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GenericResponse<Home>> call, Throwable t) {
//                loadingDialog.closeDialog();
//            }
//        });
    }


    private void renderDeviceInformation() {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                ArrayList<Device> updatedDevice = (ArrayList) bundle.get("devices");
                for (int i = 0; i < updatedDevice.size(); i++) {
                    final Device eachDevice = updatedDevice.get(i);
                    for (Device device : devices) {
                        if (device.getDeviceId().equalsIgnoreCase(eachDevice.getDeviceId())) {
                            device.setDeviceStatus(eachDevice.getDeviceStatus());
                            device.setDeviceName(eachDevice.getDeviceName());
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerNetworkChangeReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (subscriptionWatcher != null && !subscriptionWatcher.isCanceled()) {
            subscriptionWatcher.cancel();
        }
        unregisterNetworkChangeReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (subscriptionWatcher != null && !subscriptionWatcher.isCanceled()) {
            subscriptionWatcher.cancel();
        }
    }


    private void startSubscription() {
        OnLockStatusChangeSubscription subscription = OnLockStatusChangeSubscription.builder().email(email).build();
        subscriptionWatcher = ClientFactory.appSyncClient().subscribe(subscription);
        subscriptionWatcher.execute(subscriptionCallback);
    }

    private AppSyncSubscriptionCall.Callback<OnLockStatusChangeSubscription.Data> subscriptionCallback = new AppSyncSubscriptionCall.Callback<OnLockStatusChangeSubscription.Data>() {
        @Override
        public void onResponse(@Nonnull com.apollographql.apollo.api.Response<OnLockStatusChangeSubscription.Data> response) {
            Log.v("AppSync", "Subscription response: " + new Gson().toJson(response));
            OnLockStatusChangeSubscription.OnManageDevice device = response.data().onManageDevice();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    persistDeviceInformation(device);
                }
            });
        }

        @Override
        public void onFailure(final @Nonnull ApolloException e) {
            Log.v("AppSync", "Subscription failure", e);
        }

        @Override
        public void onCompleted() {
            Log.v("AppSync", "Subscription completed");
        }
    };

    private void persistDeviceInformation(OnLockStatusChangeSubscription.OnManageDevice device) {
        for (Device eachDevice : devices) {
            if (eachDevice.getDeviceId().equalsIgnoreCase(device.deviceId())) {
                eachDevice.setDeviceStatus(device.deviceStatus());
            }
        }
        adapter.notifyDataSetChanged();

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Utils.isNetworkAvailable(context)) {
                if (subscriptionWatcher != null && subscriptionWatcher.isCanceled()) {
                    startSubscription();
                }
                getUserDevices();
            } else {
                if (subscriptionWatcher != null && !subscriptionWatcher.isCanceled()) {
                    subscriptionWatcher.cancel();
                }
            }
        }
    };

    private void registerNetworkChangeReceiver() {
        registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterNetworkChangeReceiver() {
        unregisterReceiver(broadcastReceiver);
    }


    private void getUserDevices() {
        final GetUserDevicesQuery getUserDevice = GetUserDevicesQuery.builder().build();
        ClientFactory.appSyncClient() //
                .query(getUserDevice).responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK).enqueue(new GraphQLCall.Callback<GetUserDevicesQuery.Data>() {

            @Override
            public void onResponse(@Nonnull com.apollographql.apollo.api.Response<GetUserDevicesQuery.Data> response) {
                devices.clear();
                final List<GetUserDevicesQuery.GetUserDevice> allDevices = response.data().getUserDevices();
                for (GetUserDevicesQuery.GetUserDevice _device : allDevices) {
                    devices.add(new Device(_device.devicename(), _device.lockstatus(), _device.deviceid(), _device.batterypercentage(), null, _device.firmwareversion()));
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailure(@Nonnull ApolloException e) {
                e.printStackTrace();
            }
        });
    }
}