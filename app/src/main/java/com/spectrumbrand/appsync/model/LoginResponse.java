package com.spectrumbrand.appsync.model;

public class LoginResponse {

    private final boolean error;
    private final String message;
    private final String token;

    public LoginResponse(boolean error, String message, String token) {
        this.error = error;
        this.message = message;
        this.token = token;
    }

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }
}
