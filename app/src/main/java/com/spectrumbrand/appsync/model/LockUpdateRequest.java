package com.spectrumbrand.appsync.model;

public class LockUpdateRequest {

    private final String source;
    private final String action;

    public LockUpdateRequest(final String action, final String source) {
        this.action = action;
        this.source = source;
    }
}
