package com.spectrumbrand.appsync.model;

public class LoginRequest {
    private final String username;
    private final String password;

    public LoginRequest(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }
}
