package com.spectrumbrand.appsync.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Device implements Parcelable {

    @SerializedName("devicename")
    private String deviceName;
    @SerializedName("deviceid")
    private String deviceId;
    @SerializedName("lockstatus")
    private String deviceStatus;
    @SerializedName("batterypercentage")
    private Integer batteryPercentage;
    private String operationType;
    @SerializedName("firmwareversion")
    private String firmwareVersion;

    public Device(final String deviceName, final String deviceStatus, final String deviceId, final Integer batteryPercentage, final String operationType, final String firmwareVersion) {
        this.deviceName = deviceName;
        this.deviceStatus = deviceStatus;
        this.deviceId = deviceId;
        this.batteryPercentage = batteryPercentage;
        this.operationType = operationType;
        this.firmwareVersion = firmwareVersion;
    }

    protected Device(Parcel in) {
        deviceName = in.readString();
        deviceStatus = in.readString();
        deviceId = in.readString();
        batteryPercentage = in.readInt();
        operationType = in.readString();
        firmwareVersion = in.readString();
    }

    public Integer getBatteryPercentage() {
        return batteryPercentage;
    }


    public String getOperationType() {
        return operationType;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }


    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceName(final String deviceName) {
        this.deviceName = deviceName;
    }

    public void setDeviceStatus(final String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public void setBatteryPercentage(Integer batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(deviceName);
        parcel.writeString(deviceStatus);
        parcel.writeString(deviceId);
        parcel.writeInt(batteryPercentage);
        parcel.writeString(operationType);
        parcel.writeString(firmwareVersion);
    }

    public static final Creator<Device> CREATOR = new Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel in) {
            return new Device(in);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };
}
