package com.spectrumbrand.appsync.model;

import java.util.List;

public class GenericResponse<T> {

    private final List<T> data;

    public GenericResponse(final List<T> data) {
        this.data = data;
    }

    public List<?> getData() {
        return this.data;
    }
}
