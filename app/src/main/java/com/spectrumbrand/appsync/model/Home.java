package com.spectrumbrand.appsync.model;

public class Home {

    private final String homeid;
    private final String homename;

    public Home(final String homeId, final String homeName) {
        this.homeid = homeId;
        this.homename = homeName;
    }

    public String getHomeid() {
        return this.homeid;
    }

    public String getHomename() {
        return this.homename;
    }
}
