package com.spectrumbrand.appsync.api;

import com.spectrumbrand.appsync.model.Device;
import com.spectrumbrand.appsync.model.GenericResponse;
import com.spectrumbrand.appsync.model.Home;
import com.spectrumbrand.appsync.model.LockUpdateRequest;
import com.spectrumbrand.appsync.model.LoginRequest;
import com.spectrumbrand.appsync.model.LoginResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Api {

    @POST("token")
    @Headers("Content-Type: application/json")
    Call<LoginResponse> getToken(@Body LoginRequest loginRequest);

    @GET("users/me/homes")
    Call<GenericResponse<Home>> getHomes();

    @GET("homes/{id}/devices")
    Observable<GenericResponse<Device>> getDevicesOfHome(@Path("id") String id);

    @PATCH("devices/{id}/status")
    Call<Object> updateLockStatus(@Path("id") String deviceId, @Body LockUpdateRequest lockUpdateRequest);
}
