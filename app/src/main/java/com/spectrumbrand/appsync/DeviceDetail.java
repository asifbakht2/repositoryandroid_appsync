package com.spectrumbrand.appsync;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.demo.posts.GetUserDevicesQuery;
import com.amazonaws.demo.posts.OnLockStatusChangeSubscription;
import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.spectrumbrand.appsync.api.RetrofitClient;
import com.spectrumbrand.appsync.configuration.ClientFactory;
import com.spectrumbrand.appsync.configuration.Utils;
import com.spectrumbrand.appsync.model.Device;
import com.spectrumbrand.appsync.model.LockUpdateRequest;
import com.spectrumbrand.appsync.storage.SharedPrefManager;
import com.spectrumbrand.appsync.utils.LoadingDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeviceDetail extends AppCompatActivity {

    private TextView deviceNameText = null;
    private TextView deviceStatusText = null;
    private TextView deviceTypeText = null;
    private Button lockUnlockButton = null;
    private AppSyncSubscriptionCall<OnLockStatusChangeSubscription.Data> subscriptionWatcher;
    private Device currentDevice = null;
    private SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss.SSS");

    private List<Device> devices = new ArrayList<>();
    private String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);
        currentDevice = getIntent().getExtras().getParcelable("deviceObject");
        registerNetworkChangeReceiver();
        startSubscription();
        email = SharedPrefManager.getInstance(DeviceDetail.this).getCredentials().getUsername();
        if (currentDevice != null) {
            deviceNameText = (TextView) findViewById(R.id.deviceNameText);
            deviceStatusText = (TextView) findViewById(R.id.deviceStatusText);
            deviceTypeText = (TextView) findViewById(R.id.deviceTypeText);
            lockUnlockButton = (Button) findViewById(R.id.lockUnlockButton);
            deviceNameText.setText(currentDevice.getDeviceName());
            deviceStatusText.setText(currentDevice.getDeviceStatus());
            deviceTypeText.setText("N/A");
            final String action = currentDevice.getDeviceStatus().equalsIgnoreCase("Locked") ? "Unlock" : "Lock";
            final LoadingDialog loadingDialog = new LoadingDialog(DeviceDetail.this);

            lockUnlockButton.setText(action);
            lockUnlockButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String lockStatus = deviceStatusText.getText().toString().trim();

                    final Call<Object> call = RetrofitClient.getInstance().getApi().updateLockStatus(currentDevice.getDeviceId(), new LockUpdateRequest(action.toLowerCase(), "{ \"name\": \"Dummy\", \"device\": \"Android\" }"));
                    loadingDialog.showDialog("This shoudl replace message");
                    call.enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {
                            loadingDialog.closeDialog();
                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            loadingDialog.closeDialog();
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        Log.v("AppSync", "The onDestroy() event");
        setResult(Activity.RESULT_OK, new Intent().putParcelableArrayListExtra("devices", (ArrayList) devices));
        finish();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterNetworkChangeReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerNetworkChangeReceiver();
    }

    private void registerNetworkChangeReceiver() {
        registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterNetworkChangeReceiver() {
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (subscriptionWatcher != null) {
            subscriptionWatcher.cancel();
        }
    }

    private void startSubscription() {

        final OnLockStatusChangeSubscription subscription = OnLockStatusChangeSubscription.builder().email(email).build();
        subscriptionWatcher = ClientFactory.appSyncClient().subscribe(subscription);
        subscriptionWatcher.execute(subscriptionCallback);
    }


    private AppSyncSubscriptionCall.Callback<OnLockStatusChangeSubscription.Data> subscriptionCallback = new AppSyncSubscriptionCall.Callback<OnLockStatusChangeSubscription.Data>() {
        @Override
        public void onResponse(@Nonnull com.apollographql.apollo.api.Response<OnLockStatusChangeSubscription.Data> response) {
            Log.v("AppSync", "Subscription response: " + new Gson().toJson(response));
            OnLockStatusChangeSubscription.OnManageDevice device = response.data().onManageDevice();
            if (device.deviceId().equalsIgnoreCase(currentDevice.getDeviceId())) {
                currentDevice.setDeviceStatus(device.deviceStatus());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final String lockStatus = device.deviceStatus();
                        deviceStatusText.setText(lockStatus);
                        deviceTypeText.setText(sdfDate.format(new Date()));
                        lockUnlockButton.setText(lockStatus.equalsIgnoreCase("Locked") ? "Unlock" : "Lock");
                    }
                });
            }
            persistDeviceInformation(device);
        }

        @Override
        public void onFailure(final @Nonnull ApolloException e) {
            Log.v("AppSync", "Subscription failure", e);
        }

        @Override
        public void onCompleted() {
            Log.v("AppSync", "Subscription completed");
        }
    };

    private void persistDeviceInformation(OnLockStatusChangeSubscription.OnManageDevice device) {
        boolean deviceExists = false;
        for (Device eachDevice : devices) {
            if (eachDevice.getDeviceId().equalsIgnoreCase(device.deviceId())) {
                deviceExists = true;
                eachDevice.setDeviceStatus(device.deviceStatus());
            }
        }
        if (!deviceExists) {
            devices.add(new Device(device.deviceName(), device.deviceStatus(), device.deviceId(), device.batteryPercentage(), device.operationType(), device.firmwareVersion()));
        }
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Utils.isNetworkAvailable(context)) {
                if (subscriptionWatcher.isCanceled()) {
                    startSubscription();
                }
                getUserDevices();
            } else {
                if (!subscriptionWatcher.isCanceled()) {
                    subscriptionWatcher.cancel();
                }
            }
        }
    };

    private void getUserDevices() {
        final GetUserDevicesQuery getUserDevice = GetUserDevicesQuery.builder().build();
        ClientFactory.appSyncClient() //
                .query(getUserDevice).responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK).enqueue(new GraphQLCall.Callback<GetUserDevicesQuery.Data>() {

            @Override
            public void onResponse(@Nonnull com.apollographql.apollo.api.Response<GetUserDevicesQuery.Data> response) {
                devices = new ArrayList<>();
                final List<GetUserDevicesQuery.GetUserDevice> allDevices = response.data().getUserDevices();
                for (GetUserDevicesQuery.GetUserDevice _device : allDevices) {
                    if (_device.deviceid().equalsIgnoreCase(currentDevice.getDeviceId())) {
                        currentDevice.setDeviceStatus(_device.lockstatus());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final String lockStatus = _device.lockstatus();
                                deviceStatusText.setText(lockStatus);
                                deviceTypeText.setText(sdfDate.format(new Date()));
                                lockUnlockButton.setText(lockStatus.equalsIgnoreCase("Locked") ? "Unlock" : "Lock");
                            }
                        });
                    }
                    devices.add(new Device(_device.devicename(), _device.lockstatus(), _device.deviceid(), _device.batterypercentage(), null, _device.firmwareversion()));
                }
            }

            @Override
            public void onFailure(@Nonnull ApolloException e) {
                e.printStackTrace();
            }
        });
    }
}