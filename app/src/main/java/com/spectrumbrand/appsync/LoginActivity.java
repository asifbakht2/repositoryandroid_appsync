package com.spectrumbrand.appsync;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.spectrumbrand.appsync.api.RetrofitClient;
import com.spectrumbrand.appsync.configuration.ClientFactory;
import com.spectrumbrand.appsync.model.LoginRequest;
import com.spectrumbrand.appsync.model.LoginResponse;
import com.spectrumbrand.appsync.storage.SharedPrefManager;
import com.spectrumbrand.appsync.utils.LoadingDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText userNameEditText;
    private EditText passwordEditText;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userNameEditText = findViewById(R.id.username);
        passwordEditText = findViewById(R.id.password);
        userNameEditText.setText("asifbakht@gmail.com");
        passwordEditText.setText("Asif0313!");
        loginButton = findViewById(R.id.login);
        loginButton.setOnClickListener(this);
    }


    private void userLogin() {

//
//        final LoginResponse loginResponse = SharedPrefManager.getInstance(LoginActivity.this).getSession();
//        if (loginResponse != null && loginResponse.getToken() != null) {
//            RetrofitClient.getInstance().setAuthToken(loginResponse.getToken());
//            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//            return;
//        }



        final String email = userNameEditText.getText().toString().trim();
        final String password = passwordEditText.getText().toString().trim();

        if (email.isEmpty()) {
            userNameEditText.setError("Email is required");
            userNameEditText.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            userNameEditText.setError("Enter a valid email");
            userNameEditText.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            passwordEditText.setError("Password required");
            passwordEditText.requestFocus();
            return;
        }

        if (password.length() < 6) {
            userNameEditText.setError("Password should be atleast 8 character long");
            userNameEditText.requestFocus();
            return;
        }

        final LoginRequest loginRequest = new LoginRequest(email, password);
        final Call<LoginResponse> call = RetrofitClient.getInstance().getApi().getToken(loginRequest);
        final LoadingDialog loadingDialog = new LoadingDialog(LoginActivity.this);
        loadingDialog.showDialog("This shoudl replace message");

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                loadingDialog.closeDialog();
                LoginResponse loginResponse = response.body();
                if (response.body() != null) {
                    Log.v("AppSync", response.body().toString());
                    RetrofitClient.getInstance().setAuthToken(loginResponse.getToken());
                    SharedPrefManager.getInstance(LoginActivity.this).saveCredentials(loginRequest);
                    SharedPrefManager.getInstance(LoginActivity.this).saveSession(loginResponse);
                    ClientFactory.init(LoginActivity.this);
                    final Intent intent = new Intent(LoginActivity.this, DeviceActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    Toast.makeText(LoginActivity.this, "Unable to login", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loadingDialog.closeDialog();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                userLogin();
                break;
        }
    }
}