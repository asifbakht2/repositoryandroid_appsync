package com.spectrumbrand.appsync.configuration;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.spectrumbrand.appsync.model.Device;

import java.util.List;

public class Utils {

    public static Boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
