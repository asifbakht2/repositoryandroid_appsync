package com.spectrumbrand.appsync.configuration;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.amazonaws.regions.Regions;
import com.spectrumbrand.appsync.storage.SharedPrefManager;

public class ClientFactory {

    private static volatile AWSAppSyncClient client;

    public static synchronized void init(final Context context) {
        if (client == null) {
            final AWSConfiguration awsConfiguration = new AWSConfiguration(context);
            final CognitoUserPoolsAuthProvider k = new CognitoUserPoolsAuthProvider() {
                @Override
                public String getLatestAuthToken() {
                    try {
                        return SharedPrefManager.getInstance(context).getSession().getToken();
                    } catch (Exception e) {
                        Log.e("APPSYNC_ERROR", e.getLocalizedMessage());
                        return e.getLocalizedMessage();
                    }
                }
            };
            awsConfiguration.setConfiguration("dev");
            client = AWSAppSyncClient.builder()
                    .context(context)
                    .region(Regions.US_EAST_1)
                    .serverUrl("https://izcnuw5xvfe2xkrxkcj2se3zui.appsync-api.us-east-1.amazonaws.com/graphql")
                    .cognitoUserPoolsAuthProvider(new CognitoUserPoolsAuthProvider() {
                        @Override
                        public String getLatestAuthToken() {
                            try {
                                return SharedPrefManager.getInstance(context).getSession().getToken();
                            } catch (Exception e) {
                                Log.e("APPSYNC_ERROR", e.getLocalizedMessage());
                                return e.getLocalizedMessage();
                            }
                        }
                    }).build();
        }
    }

    public static synchronized AWSAppSyncClient appSyncClient() {
        return client;
    }
}